﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

namespace OpenLinq.Tests.Helper
{
    public class ThrowingEnumerable<T> : IEnumerable<T>
    {
        public IEnumerator<T> GetEnumerator()
        {
            throw new InvalidOperationException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static void AssertDeferred<TSource, TResult>(
            Func<IEnumerable<TSource>, IEnumerable<TResult>> deferredFunction)
        {
            var source = new ThrowingEnumerable<TSource>();
            var result = deferredFunction(source);

            using (var iterator = result.GetEnumerator())
            {
                Assert.Throws<InvalidOperationException>(() => iterator.MoveNext());
            }
        }
    }
}
