﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenLinq.Tests.Helper;

namespace OpenLinq.Tests
{
    [TestFixture]
    public class Concat
    {
        [Test]
        public void FirstSequenceIsntAccessedBeforeFirstUse()
        {
            IEnumerable<int> first = new ThrowingEnumerable<int>();
            IEnumerable<int> second = new int[] { 5 };
            // No exception yet...
            var query = first.Concat(second);
            // Still no exception...
            using (var iterator = query.GetEnumerator())
            {
                // Now it will go bang
                Assert.Throws<InvalidOperationException>(() => iterator.MoveNext());
            }
        }
        [Test]
        public void SecondSequenceIsntAccessedBeforeFirstUse()
        {
            IEnumerable<int> first = new int[] { 5 };
            IEnumerable<int> second = new ThrowingEnumerable<int>();
            // No exception yet...
            var query = first.Concat(second);
            // Still no exception...
            using (var iterator = query.GetEnumerator())
            {
                // First element is fine...
                Assert.IsTrue(iterator.MoveNext());
                Assert.AreEqual(5, iterator.Current);
                // Now it will go bang, as we move into the second sequence
                Assert.Throws<InvalidOperationException>(() => iterator.MoveNext());
            }
        }
    }
}
