﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenLinq.Tests.Extensions;
using OpenLinq.Tests.Helper;

namespace OpenLinq.Tests
{
    [TestFixture]
    class Where
    {
        [Test]
        public void SimpleFiltering()
        {
            int[] source = {1, 3, 4, 2, 8, 1};
            var result = source.Where(x => x < 4);
            result.AssertSequenceEqual(1, 3, 2, 1);
        }

        [Test]
        public void NullSourceThrowsNullArgumentException()
        {
            IEnumerable<int> source = null;
            Assert.Throws<ArgumentNullException>(() => source.Where(x => x > 5));
        }

        [Test]
        public void NullPredicateThrowsNullArgumentException()
        {
            int[] source = { 1, 3, 7, 9, 10 };
            Func<int, bool> predicate = null;
            Assert.Throws<ArgumentNullException>(() => source.Where(predicate));
        }

        [Test]
        public void ExecutionIsDeferred()
        {
            ThrowingEnumerable<int>.AssertDeferred<int, int>(src => src.Where(x => x > 0));
        }
    }
}
