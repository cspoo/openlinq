﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OpenLinq
{
    public static partial class Enumerable
    {
        public static IEnumerable<TResult> Empty<TResult>()
        {
            return EmptyEnumerable<TResult>.Instance;
        }

        private class EmptyEnumerable<T> : IEnumerable<T>, IEnumerator<T>
        {
            internal static readonly IEnumerable<T> Instance = new EmptyEnumerable<T>();
 
            private EmptyEnumerable()
            {
            }


            public IEnumerator<T> GetEnumerator()
            {
                return this;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this;
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                return false;
            }

            public void Reset()
            {
            }

            public T Current
            {
                get
                {
                    throw new InvalidOperationException();
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}
