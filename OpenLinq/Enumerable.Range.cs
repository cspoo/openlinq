﻿using System;
using System.Collections.Generic;

namespace OpenLinq
{
    public static partial class Enumerable
    {
        public static IEnumerable<int> Range(int start, int count)
        {
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            if ((long) start + (long) count - 1L > int.MaxValue)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            return RangeImpl(start, count);
        }

        private static IEnumerable<int> RangeImpl(int start, int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return start + i;
            }
        }
    }
}
